package ir.manoosheh.opdm.services.data;

import ir.manoosheh.opdm.models.Amount;
import ir.manoosheh.opdm.repositories.AmountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmountService {
    @Autowired
    AmountRepository amountRepository;

    public String addAmount(Amount amount){
        String message = "";
        if(amountRepository.addAmount(amount))
            message = "Amonet added successfully!";
        return message;
    }
}
