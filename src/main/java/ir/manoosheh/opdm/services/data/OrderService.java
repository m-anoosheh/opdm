package ir.manoosheh.opdm.services.data;

import ir.manoosheh.opdm.models.EOrderStatus;
import ir.manoosheh.opdm.models.User;

import java.util.Optional;

public class OrderService {
    public Optional<EOrderStatus> getOrderSatusFromString(String status){
        Optional<EOrderStatus> opt = Optional.ofNullable(EOrderStatus.valueOf(status.toUpperCase()));
        return opt;
    }
}
