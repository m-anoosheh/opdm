package ir.manoosheh.opdm.services.data;

import ir.manoosheh.opdm.models.*;
import ir.manoosheh.opdm.repositories.ProductRepository;
import ir.manoosheh.opdm.repositories.UserRepository;
import ir.manoosheh.opdm.request.product.ProductRequest;
import ir.manoosheh.opdm.request.user.DeleteRequest;
import ir.manoosheh.opdm.responce.ProductBasicInfo;
import ir.manoosheh.opdm.responce.UserBasicInfo;
import ir.manoosheh.opdm.security.jwt.request.SignupRequest;
import ir.manoosheh.opdm.request.user.UpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductService productService;



    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleService roleService;


    public String addUser(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            if(userRepository.existUser(signUpRequest.getUsername()))
                return "Error: User is already exist!";
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            if(userRepository.existUser(signUpRequest.getEmail()))
                return "Error: User is already exist!";
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));
        user.setRoles(roleService.getRolesFromString(signUpRequest.getRole()));
        userRepository.addUser(user);
        return "User registered successfully!";
    }
    public String updateUserByAdmin(UUID userID, SignupRequest signupRequest){
        User user = userRepository.findById(userID)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with userID: " + userID));
        user.setUsername(signupRequest.getUsername());
        user.setEmail(signupRequest.getEmail());
        user.setPassword(encoder.encode(signupRequest.getPassword()));
        user.setExist(true);
        user.setRoles(roleService.getRolesFromString(signupRequest.getRole()));
        userRepository.updateUser(user);
        return "User updated successfully!";
    }
    public String updateUser(SignupRequest signupRequest){
        User user;
        if(userRepository.findByUsername(signupRequest.getUsername()).isPresent()){
            user = userRepository.findByUsername(signupRequest.getUsername()).get();
        }
        else
        {
            return "User not found!";
        }
        User updateUser = userRepository.findById(user.getId()).get();

        updateUser.setUsername(signupRequest.getUsername());
        updateUser.setEmail(signupRequest.getEmail());
        updateUser.setPassword(encoder.encode(signupRequest.getPassword()));
        updateUser.setExist(true);

        Set<String> strRoles = signupRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleService.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            for(String role:strRoles) {
                //strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleService.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;

                    default:
                        Role userRole = roleService.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
             //});
            }
        }
        updateUser.setRoles(roles);
        userRepository.updateUser(updateUser);
        return "User updated successfully!";
    }
    public String updateUser(User user , UpdateRequest updateRequest) {

        user.setUsername(updateRequest.getUsername());
        user.setPassword(encoder.encode(updateRequest.getPassword()));
        user.setEmail(updateRequest.getEmail());
        userRepository.updateUser(user);
        return "User updated successfully!";
    }
    public String deleteUser(UUID id){
        User user;
        userRepository.findById(id).ifPresent((x) -> {userRepository.deleteUser(x);return;});

               // message = "User deleted successfully!";

        return  "Error: User not found!";
    }
    public String addProduct(User user , List<ProductRequest> productRequests){
        return productService.addProduct(user.getId(),productRequests);
    }
    public String addProduct(User user, ProductRequest productRequest){
        String message = "";

        /*List<Product> products = productService.getAllproducts(user);//.getAllproducts(user);
            if(!products.isEmpty()) {
                for (Product product : products) {
                    if (product.getProductname().equals(productRequest.getProductname()) && product.isExist()) {
                        //update amount
                        return productService.updateProduct(product, productRequest);
                    }
                }
            }*/


        return productService.addProduct(user,productRequest);

    }
    public String deleteProduct(User user, ProductRequest productRequest){
        String message = "";
        /*List<Product> products = new ArrayList<>();
        products = productService.getAllproducts(user);
        for(Product product:products)
        {
            if(product.getProductname().equals(productRequest.getProductname())){
                //update amount
                return productService.deleteProduct(product);
            }

        }*/
        return "Product not found!";

    }
    public List<ProductBasicInfo> getAllProductBasicInfo(User user){
        List<ProductBasicInfo> products;
        products = productService.getAllproductBasicInfo(user);
        return products;
    }
    public Boolean existsByUsername(String username){
        return userRepository.existsByUsername(username);
    }
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    public Optional<User> findByemail(String email){
        return userRepository.findByEmail(email);
    }

    public List<User> getAllUsers() {
        List<User> users = userRepository.getAllUser();
        /*for(User user:users){

            userRepository.setUserRoles(user);
            productService.setAllproducts(user);

        }*/
        return users;
    }

    public Optional<User> findById(UUID id) {
        return userRepository.findById(id);
    }

   /* public List<Role> getUserRoles(User user){
        userRepository.getUserRoles(user);
        return null;
    }*/

    public List<UserBasicInfo> getAllUserBasicInfo() {
        List<UserBasicInfo> userBasicInfos = new ArrayList<>();
        /*for(User user:userRepository.getAllUser()){
            *//*List<Role> roles = new ArrayList<>();
            roles = userRepository.getUserRoles(user).get();*//*
            userBasicInfos.add(new UserBasicInfo(user.getId()
                                                ,user.getUsername()
                                                ,user.getEmail()

                                                ));
        }*/
        return userBasicInfos;
    }

    public List<Product> getAllUserProducts(User user) {
        return productService.getAllUserproducts(user);
    }
}
