package ir.manoosheh.opdm.services.data;

import ir.manoosheh.opdm.models.Amount;
import ir.manoosheh.opdm.models.Product;
import ir.manoosheh.opdm.models.User;
import ir.manoosheh.opdm.repositories.ProductRepository;
import ir.manoosheh.opdm.request.product.ProductRequest;
import ir.manoosheh.opdm.responce.ProductBasicInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private AmountService amountService;

    public String addProduct(User user, ProductRequest productRequest){
        String message="";

        productRepository.addProduct(user,productRequest);
        message ="Product(s) added successfully!";

        return message;
    }

    public String addProduct(UUID userId, List<ProductRequest> productRequests){
        return productRepository.addProduct(userId,productRequests);
    }
    public String updateProduct(Product product , ProductRequest productRequest){

        String message="";
        //productRepository.addProduct(user,productRequest);

        Amount amount = new Amount(productRequest.getAmount());
        product.addAmount(amount);
        amountService.addAmount(amount);

        message ="Product(s) updated successfully!";

        return null;
    }

    public String deleteProduct(Product product){
        String message="";
        productRepository.deleteProduct(product);

        message ="Product(s) deleted successfully!";

        return message;
    }

    public List<Product> setAllproducts(User user){
        return  productRepository.setAllProducts(user);
    }
    public List<Product> getAllUserproducts(User user){
        return  productRepository.getAllUserProducts(user);
    }
    public List<ProductBasicInfo> getAllproductBasicInfo(User user) {
        List<ProductBasicInfo> productBasicInfos = new ArrayList<>();
       /* List<Product> products = productRepository.getAllProducts(user.getId());
        for(Product product:products){
            if(!product.isExist())
                continue;
            ProductBasicInfo  item = new ProductBasicInfo();
            item.setId(product.getId());
            item.setProductname(product.getProductname());
            productBasicInfos.add(item);

        }*/
        return productBasicInfos;
    }
}
