package ir.manoosheh.opdm.services.data;

import ir.manoosheh.opdm.models.ERole;
import ir.manoosheh.opdm.models.Role;
import ir.manoosheh.opdm.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public void addRole(Role role){
        roleRepository.addRole(role);
    }
    public Optional<Role> findByName(ERole name){
        return roleRepository.findByName(name);

    }
    public Set<Role> getRolesFromString(Set<String> strRoles){
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            for(String role:strRoles) {
                //strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;

                    default:
                        Role userRole = findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
                //});
            }
        }
        return roles;
    }
}
