package ir.manoosheh.opdm.request.product;

import javax.validation.constraints.NotBlank;

public class ProductRequest {

    @NotBlank
    private String productname;

    private long amount;

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
