package ir.manoosheh.opdm.request.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

public class AdminSelectUserRequest {
    @NotBlank
    @Size(min = 3, max = 40)
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
