package ir.manoosheh.opdm.repositories;

import ir.manoosheh.opdm.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.*;


@Repository
public class UserRepository  {
    @Autowired
    private SessionFactory sessionFactory;



    @Transactional
    public boolean addUser(User user) {
            Session session = sessionFactory.getCurrentSession();
            UUID id = (UUID) session.save(user);
            if(id.toString().length()>0)
                return true;
            else
                return false;
    }
    @Transactional
    public void updateUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);

    }
    @Transactional
    public void deleteUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("UPDATE User SET exist=:exist WHERE id = :id ")
                .setParameter("exist",false)
                .setParameter("id",user.getId())
                .executeUpdate();

    }


    @Transactional
    public List<User> getAllUser(){
        Session session = sessionFactory.getCurrentSession();
        List<UUID> ids = session.createQuery("SELECT a.id FROM User a", UUID.class).getResultList();
        //List<User> user = session.createQuery("SELECT a FROM User a", User.class).getResultList();
        List<User> users = new ArrayList<>();
        List<Object> userObjects = new ArrayList<>();
        for(UUID id : ids){
            User user = new User();
            User tmpUser = session.get(User.class,id);
            user.setId(tmpUser.getId());
            user.setUsername(tmpUser.getUsername());
            user.setEmail(tmpUser.getEmail());
            user.setExist(tmpUser.isExist());
            Set<Role> roles = new HashSet();
            for(Role tmpRole:tmpUser.getRoles()){
                Role role = new Role();
                role.setId(tmpRole.getId());
                role.setName(tmpRole.getName());
                roles.add(role);
            }
            user.setRoles(roles);
            List<Product> products = new ArrayList<>();
            for(Product tmpProduct:tmpUser.getProducts()){
                Product product = new Product();
                product.setId(tmpProduct.getId());
                product.setProductname(tmpProduct.getProductname());
                product.setExist(tmpProduct.isExist());
                List<Amount> amounts = new ArrayList<>();
                for(Amount tmpAmount : tmpProduct.getAmount()){
                    Amount amount = new Amount();
                    amount.setId(tmpAmount.getId());
                    amount.setValue(tmpAmount.getValue());
                    amounts.add(amount);
                }
                product.setAmount(amounts);
                products.add(product);
            }
            user.setProducts(products);
            List<Order> orders = new ArrayList<>();
            for(Order tmpOrder:tmpUser.getOrders()){
                Order order = new Order();
                order.setId(tmpOrder.getId());
                order.setOrdername(tmpOrder.getOrdername());
                order.setStatus(tmpOrder.getStatus());
                List<Product> productsInorder = new ArrayList<>();
                for(Product tmpProduct:tmpUser.getProducts()){
                    Product product = new Product();
                    product.setId(tmpProduct.getId());
                    product.setProductname(tmpProduct.getProductname());
                    product.setExist(tmpProduct.isExist());

                    productsInorder.add(product);
                }
                order.setProducts(productsInorder);
                orders.add(order);

            }
            user.setOrders(orders);

            users.add(user);
        }
        //String jsonUsers = new Gson().toJson(users);
        //Optional<String> opt = Optional.ofNullable(jsonUsers);
        return users;
    }
    @Transactional
    public Boolean existsByUsername(String username){
        boolean result=false;
        result = findByUsername(username).isPresent();
        return result;
    }

    @Transactional
    public Boolean existsByEmail(String email){
        boolean result=false;
        result = findByEmail(email).isPresent();
        return result;
    }
    @Transactional
    public boolean existUser(String username){
        Session session = sessionFactory.getCurrentSession();
        User user =  (User)session.createQuery("FROM User WHERE username = :username ")
                .setParameter("username",username)
                .uniqueResult();
        return user.isExist();
    }
    @Transactional
    public Optional<User> findByUsername(String username){
        Session session = sessionFactory.getCurrentSession();
        User user =  (User)session.createQuery("FROM User WHERE username = :username ")
                .setParameter("username",username)
                .uniqueResult();
        Optional<User> opt = Optional.ofNullable(user);
        return opt;

    }
    @Transactional
    public Optional<User> findById(UUID id){
        Session session = sessionFactory.getCurrentSession();
        User user =  (User)session.createQuery("FROM User WHERE id = :id ")
                .setParameter("id",id)
                .uniqueResult();
        Optional<User> opt = Optional.ofNullable(user);
        return opt;
    }
    @Transactional
    public Optional<User> findByEmail(String email){
        Session session = sessionFactory.getCurrentSession();
        User user =  (User)session.createQuery("FROM User WHERE email = :email ")
                .setParameter("email",email)
                .uniqueResult();
        Optional<User> opt = Optional.ofNullable(user);
        return opt;
    }

    @Transactional
    public void setUserRoles(User user) {
        Session session = sessionFactory.getCurrentSession();
        Set<Role> roles = new HashSet<>() ;
        User tmpUser2 = session.get(User.class,user.getId());

        User tmpUser = (User)session.createQuery("select distinct user from User user " +
                                                    "LEFT join user.roles role where user.id = :userid")
                        .setParameter("userid", user.getId())
                        .uniqueResult();
        user.setRoles(tmpUser.getRoles());
               // .getResultList();

    }
}
