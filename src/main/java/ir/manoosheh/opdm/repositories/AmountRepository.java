package ir.manoosheh.opdm.repositories;

import ir.manoosheh.opdm.models.Amount;
import ir.manoosheh.opdm.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.UUID;

@Repository
public class AmountRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public boolean addAmount(Amount amount) {
        Session session = sessionFactory.getCurrentSession();
        UUID id = (UUID) session.save(amount);
        if(id.toString().length()>0)
            return true;
        else
            return false;
    }
}
