package ir.manoosheh.opdm.repositories;

import ir.manoosheh.opdm.models.ERole;
import ir.manoosheh.opdm.models.Role;
import ir.manoosheh.opdm.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public class RoleRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public void addRole(Role role) {
        Session session = sessionFactory.getCurrentSession();
        session.save(role);
    }

    @Transactional
    public Optional<Role> findByName(ERole name){
        Session session = sessionFactory.getCurrentSession();
        Role role =  (Role)session.createQuery("FROM Role WHERE name = :name ")
                .setParameter("name",name)
                .uniqueResult();
        return Optional.ofNullable(role);
    }
}
