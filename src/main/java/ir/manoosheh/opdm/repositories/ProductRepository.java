package ir.manoosheh.opdm.repositories;

import ir.manoosheh.opdm.models.Amount;
import ir.manoosheh.opdm.models.Product;
import ir.manoosheh.opdm.models.User;
import ir.manoosheh.opdm.request.product.ProductRequest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.*;

@Repository
public class ProductRepository {
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private AmountRepository amountRepository;

    @Transactional
    public String addProduct(UUID userId, List<ProductRequest> productRequests) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class,userId);
        boolean isRepetitive = false;
        for(ProductRequest productRequest:productRequests){
            isRepetitive = false;
            for(Product product:user.getProducts()){
                if(product.getProductname().equals(productRequest.getProductname())){
                    Amount amount = new Amount(productRequest.getAmount());
                    amount.setProductObject(product);
                    session.save(amount);
                    isRepetitive = true;
                    break;
                }
            }
            if(!isRepetitive){
                Product product = new Product(productRequest.getProductname());
                Amount amount = new Amount(productRequest.getAmount());
                product.addAmount(amount);
                user.addProduct(product);
                session.save(amount);
                session.save(product);
            }
        }
        return "All products have been added";
    }

        @Transactional
    public boolean addProduct(User user, ProductRequest productRequest) {
        Session session = sessionFactory.getCurrentSession();
        Amount amount = new Amount(productRequest.getAmount());
        Product product = new Product(productRequest.getProductname());
        user.addProduct(product);
        product.addAmount(amount);
        UUID id = (UUID) session.save(product);
        UUID id2 = (UUID) session.save(amount);
        if(id.toString().length()>0)
            return true;
        else
            return false;
    }
    @Transactional
    public void updateProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.update(product);
    }
    @Transactional
    public void deleteProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("UPDATE Product SET exist=:exist WHERE id = :id ")
                .setParameter("exist",false)
                .setParameter("id",product.getId())
                .executeUpdate();

    }

    @Transactional
    public List<Product> setAllProducts(User user) {
        Session session = sessionFactory.getCurrentSession();
        List<Product> products;
        products = ((User)session.createQuery("select distinct user from User user " +
                "LEFT join user.products product where user.id = :userid")
                .setParameter("userid",user.getId())
                .uniqueResult()).getProducts();
        user.setProducts(products);
        return products;
    }
    @Transactional
    public List<Product> getAllProducts(User user) {
        Session session = sessionFactory.getCurrentSession();
        List<Product> products;
        products = ((User)session.createQuery("select distinct user from User user " +
                "LEFT join user.products product where user.id = :userid")
                .setParameter("userid",user.getId())
                .uniqueResult()).getProducts();

        return products;
    }

    public List<Product> getAllUserProducts(User user) {
        Session session = sessionFactory.getCurrentSession();
        User updatedUser = session.get(User.class,user.getId());
        return updatedUser.getProducts();
    }
}
