package ir.manoosheh.opdm.controllers;


import ir.manoosheh.opdm.models.User;
import ir.manoosheh.opdm.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/opdm")
public class OpdmController {
    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String userAccess() {
        return "User Content.";
    }


    @Autowired
    UserRepository userRepository;

    @GetMapping("/admin")
    //@PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(!userRepository.findByUsername(auth.getName()).isPresent()) {
            return "Admin Board.";
        }
        else {
            User user = userRepository.findByUsername(auth.getName()).get();
            return "Admin Board." + " , " + user.getUsername() + " , " + user.getId() + " , " +
                    user.getEmail();
        }
    }
}