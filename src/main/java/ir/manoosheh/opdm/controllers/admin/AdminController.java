package ir.manoosheh.opdm.controllers.admin;

import ir.manoosheh.opdm.models.User;
import ir.manoosheh.opdm.request.user.AdminSelectUserRequest;
import ir.manoosheh.opdm.request.user.AdminUpdateRequest;
import ir.manoosheh.opdm.responce.UserBasicInfo;
import ir.manoosheh.opdm.security.jwt.request.SignupRequest;
import ir.manoosheh.opdm.security.jwt.response.MessageResponse;
import ir.manoosheh.opdm.services.data.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/opdm/admin")
public class AdminController {

    @Autowired
    UserService userService;

    @PostMapping("/adduser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> addUserByAdmin(@Valid @RequestBody SignupRequest signUpRequest) {
        String message = userService.addUser(signUpRequest);
        if(message == "User registered successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));
    }

    @PostMapping("/updateuser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateUserByAdmin(@Valid @RequestBody AdminUpdateRequest adminUpdateRequest) {


        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername(adminUpdateRequest.getUsername());
        signupRequest.setEmail(adminUpdateRequest.getEmail());
        signupRequest.setPassword(adminUpdateRequest.getPassword());
        signupRequest.setRole(adminUpdateRequest.getRole());
        String message = userService.updateUserByAdmin(adminUpdateRequest.getId(),signupRequest);
        if(message == "User updated successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));
    }

    @PostMapping("/deleteuser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteUserByAdmin(@Valid @RequestBody AdminSelectUserRequest adminSelectUserRequest) {
        String message = userService.deleteUser(adminSelectUserRequest.getId());
        if(message == "User deleted successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    //@Transactional
    @ResponseBody
    public ResponseEntity<List<User>> getAllUserByAdmin() {

        List<User> users = userService.getAllUsers();

        return ResponseEntity.ok(users);

    }

    @GetMapping("/alluser")
    @PreAuthorize("hasRole('ADMIN')")
    //@ResponseBody
    public ResponseEntity<List<UserBasicInfo>> getAllUserBasicInfoByAdmin() {

        List<UserBasicInfo> t = userService.getAllUserBasicInfo();
        return ResponseEntity.ok(t);

    }

}