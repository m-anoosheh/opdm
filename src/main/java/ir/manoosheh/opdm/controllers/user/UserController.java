package ir.manoosheh.opdm.controllers.user;

import ir.manoosheh.opdm.models.Amount;
import ir.manoosheh.opdm.models.Product;
import ir.manoosheh.opdm.models.Role;
import ir.manoosheh.opdm.models.User;
import ir.manoosheh.opdm.request.product.ProductRequest;
import ir.manoosheh.opdm.request.user.AdminSelectUserRequest;
import ir.manoosheh.opdm.request.user.DeleteRequest;
import ir.manoosheh.opdm.request.user.UpdateRequest;
import ir.manoosheh.opdm.responce.ProductBasicInfo;
import ir.manoosheh.opdm.security.jwt.request.SignupRequest;
import ir.manoosheh.opdm.security.jwt.response.MessageResponse;
import ir.manoosheh.opdm.services.data.ProductService;
import ir.manoosheh.opdm.services.data.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/opdm/user")
public class UserController {
    @Autowired
    private UserService userService;


    @PostMapping("/updateme")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateUserByUser(@Valid @RequestBody UpdateRequest updateRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName()).get();
        if(updateRequest.getId() == null )
            if( !user.getId().equals(updateRequest.getId()))
                return ResponseEntity.badRequest().body("User Id must be entered and the same as Token owner!");

        String message = userService.updateUser(user,updateRequest);
        if (message == "User updated successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));

    }

    @PostMapping("/deleteuser")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> deleteUserByUser(@Valid @RequestBody AdminSelectUserRequest adminSelectUserRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName()).get();
        if( !user.getId().equals(adminSelectUserRequest.getId()))
            return ResponseEntity.badRequest().body("User Id must be the same as Token owner!");
        String message = userService.deleteUser(adminSelectUserRequest.getId());
        if(message == "User deleted successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));

    }

    @PostMapping("/addproduct")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> addProductByUser(@Valid @RequestBody List<ProductRequest> productRequests){
        String message ="";
        /*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName()).get();
        for(ProductRequest productRequest:productRequests){
            message = userService.addProduct(user,productRequest);
        }*/
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = userService.findByUsername(auth.getName()).get();
        message = userService.addProduct(user , productRequests);

        if(message == "Product(s) added successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));
    }

    @PostMapping("/deleteproduct")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> deleteProductByUser(@Valid @RequestBody ProductRequest productRequest){
        String message = "";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName()).get();
        message = userService.deleteProduct(user,productRequest);
        return ResponseEntity.ok(new MessageResponse(message));
    }

    @GetMapping("/getmyproducts")
    @PreAuthorize("hasRole('USER')")
    @Transactional
    public ResponseEntity<List<Product>> getUserProducts(){
        //List<Product> products;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName()).get();
        List<Product> products = new ArrayList<>();
        for(Product product:user.getProducts()){
            Product tmpProduct = new Product();
            tmpProduct.setProductname(product.getProductname());
            tmpProduct.setId(product.getId());
            List<Amount> amountList = new ArrayList<>();
            for(Amount amount : product.getAmount()) {
                Amount tmpAmount = new Amount(amount.getValue());
                tmpAmount.setId(amount.getId());
                amountList.add(tmpAmount);
            }
            tmpProduct.setAmount(amountList);
            products.add(tmpProduct);
        }

        return ResponseEntity.ok(products);
    }

    @PostMapping("/listproduct")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<List<ProductBasicInfo>> listProducts(){
        //List<Product> products;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName()).get();
        List<ProductBasicInfo> products = userService.getAllProductBasicInfo(user);

        return ResponseEntity.ok(products);
    }
}
