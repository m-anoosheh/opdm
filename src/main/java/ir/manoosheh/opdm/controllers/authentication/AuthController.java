package ir.manoosheh.opdm.controllers.authentication;

import ir.manoosheh.opdm.models.Role;
import ir.manoosheh.opdm.security.jwt.JwtUtils;
import ir.manoosheh.opdm.security.jwt.request.LoginRequest;
import ir.manoosheh.opdm.security.jwt.request.SignupRequest;
import ir.manoosheh.opdm.security.jwt.response.JwtResponse;
import ir.manoosheh.opdm.security.jwt.response.MessageResponse;
import ir.manoosheh.opdm.security.services.UserDetailsImpl;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import ir.manoosheh.opdm.services.data.RoleService;
import ir.manoosheh.opdm.services.data.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    @PostMapping("/addrole")
    public ResponseEntity<?> addRole(@Valid @RequestBody List<Role> roles) {
        for(Role role:roles) {
            roleService.addRole(role);
        }
        return ResponseEntity.ok(new MessageResponse("Role created successfully!"));

    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        String message = userService.addUser(signUpRequest);
        if(message == "User registered successfully!")
            return ResponseEntity.ok(new MessageResponse(message));
        else
            return ResponseEntity.badRequest().body(new MessageResponse(message));
    }
}