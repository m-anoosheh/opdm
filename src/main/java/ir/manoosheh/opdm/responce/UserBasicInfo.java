package ir.manoosheh.opdm.responce;

import ir.manoosheh.opdm.models.Product;
import ir.manoosheh.opdm.models.Role;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.*;

public class UserBasicInfo {


    private UUID id ;

    private String username;

    private String email;

    public UserBasicInfo(UUID id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
       // this.roles = roles;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /*public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }*/
}
