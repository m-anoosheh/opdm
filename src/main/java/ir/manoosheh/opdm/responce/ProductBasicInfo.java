package ir.manoosheh.opdm.responce;

import ir.manoosheh.opdm.models.Amount;
import ir.manoosheh.opdm.models.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductBasicInfo {


    private UUID id ;
    private String productname;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }
}

