package ir.manoosheh.opdm.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(	name = "products")
//@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
//@JsonIdentityInfo(generator= ObjectIdGenerators.None.class )
public class Product implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id = UUID.randomUUID();

    @NotBlank
    @Size(max = 20)
    private String productname;

    //@JsonIgnore
    @OneToMany(mappedBy = "productObject")
    private List<Amount> amount = new ArrayList<>();

    private boolean exist;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="user_id")
    User userObject;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="order_id")
    User orderObject;


    public Product() {
        this.exist = true;
    }

    public Product(@NotBlank @Size(max = 20) String productname, List<Amount> amount) {
        this.productname = productname;
        this.amount = amount;
        this.exist = true;
    }

    public Product(@NotBlank @Size(max = 20) String productname) {
        this.productname = productname;
        this.exist = true;
    }

    public void addAmount(Amount amount){
        List<Amount> amountList = new ArrayList<>();
        amount.setProductObject(this);
        this.setAmount(amountList);
    }



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public List<Amount> getAmount() {
        return amount;
    }

    public void setAmount(List<Amount> amount) {
        this.amount = amount;
    }

    public User getUser() {
        return userObject;
    }

    public void setUser(User user) {
        this.userObject = user;
    }
    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }
}
