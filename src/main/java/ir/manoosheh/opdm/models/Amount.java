package ir.manoosheh.opdm.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Entity
@Table(name = "amount")
public class Amount {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id = UUID.randomUUID();

    @NotBlank
    private long value;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="product_id")
    private Product productObject;

    //@JsonIgnore
    private boolean exist;

    public Amount() {
          this.exist = true;
    }


    public Amount(@NotBlank long value) {
        this.exist = true;
        this.value = value;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
    public Product getProductObject() {
        return productObject;
    }

    public void setProductObject(Product product) {
        this.productObject = product;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }
}
