package ir.manoosheh.opdm.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}