package ir.manoosheh.opdm.models;

public enum EOrderStatus {
    WAITING,
    PACKING,
    DELIVERING,
    DELIVERED,
    CANCELED
}
