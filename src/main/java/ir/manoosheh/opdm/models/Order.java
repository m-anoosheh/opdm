package ir.manoosheh.opdm.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id = UUID.randomUUID();

    @NotBlank
    @Size(max = 20)
    private String ordername;

    private String destination;

    @Enumerated(EnumType.STRING)
    @Size(max = 10)
    private EOrderStatus status;

    @JsonIgnore
    @OneToMany(mappedBy = "orderObject")
    private List<Product> products = new ArrayList<>();

    //@JsonIgnore
    private boolean exist;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="user_id")
    User userObject;


    public Order() {
        this.exist = true;
        this.status = EOrderStatus.WAITING;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public EOrderStatus getStatus() {
        return status;
    }

    public void setStatus(EOrderStatus status) {
        this.status = status;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }

    public User getUserObject() {
        return userObject;
    }

    public void setUserObject(User userObject) {
        this.userObject = userObject;
    }
}
